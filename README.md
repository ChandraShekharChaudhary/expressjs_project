# Setting up an Express CURD Todo App 

## Instructions of Development

### Prerequisites install

## Steps

### 1.Install and initialize an prisma

For installing all the project dependencies use these commonds in the project root directory
```
npm install 
```

### 2. Install Prisma and its dependencies and yup

In your terminal, navigate to the root directory of your app and run the following command to install Prisma and its dependencies:
```
npm install prisma @prisma/client dotenv

npm install yup
```
This will install Prisma, the Prisma client, and dotenv, which we will use to configure our environment variables.


### 3. Set up a Postgres database

Create a new Postgres database using your preferred method. Once you have created the database, create a new .env file in the root directory of your app and add the environment variables, as specified in .env.example file, replacing the placeholders with your database credentials:



### 4. Run database migrations (if the project's migrations folder is empty or if you encounter any database-related issues)

```
npx prisma migrate dev --name init
```

### 5. Generate Prisma client based on your Prisma schema

In your terminal, run the following command to generate the Prisma client based on your schema:

```
npx prisma generate
```
This will create a new node_modules/@prisma/client directory containing the Prisma client.


### 6. Add environment variables

Add the following environment variables in .env file

```
PG_USER= your_user 
PG_PASSWORD= your_password 
PG_HOST= your_host
PG_PORT= your_port_number
PG_DATABASE= your_db_name
```

### 7. Create Database

Add your existing database
Run this command to add existing database

```
npx prisma db pull
```

### 8. For run the server use 

```
npm run dev 
```
