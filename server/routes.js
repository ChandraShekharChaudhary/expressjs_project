const { Router } = require("express");
const controller = require("./controller");

const router = Router();

router.get("/", controller.getTodos);
router.post("/", controller.addTodo);
router.get("/:id", controller.getTodoByID);
router.delete("/:id", controller.removeTodo);
router.put("/:id", controller.updateTodo);

module.exports = router;
