const { PrismaClient } = require("@prisma/client");
const errorHandler = require("./errorHandler");
const yup = require("yup");

const prisma = new PrismaClient({
  datasources: {
    db: {
      url: `postgresql://${process.env.dev_user}:${process.env.dev_password}@${process.env.dev_host}:${process.env.dev_port}/${process.env.dev_database}`,
    },
  },
});

//Vailidation for data getting from body.
const dataSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean().required(),
});

// Vaididation of id.
const validateID = yup.object().shape({
  inputID: yup.number().required(),
});

//1. Get all Todos.
async function getTodos(req, res) {
  try {
    const todos = await prisma.todos.findMany();
    res.send(todos);
  } catch (err) {
    next(err);
  }
}

//2. Get a todo using its id.
async function getTodoByID(req, res, next) {
  const id = parseInt(req.params.id);
  try {
    await validateID.validate({ inputID: id });
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });
    if (!todo) {
      return res.status(404).json({ message: "Todo is not found." });
    }
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

// 3. Add a todo in the todos.
async function addTodo(req, res, next) {
  try {
    await dataSchema.validate(req.body);
  } catch (err) {
    return res.status(400).json({ message: "Invalid Input" });
  }

  const { text, iscompleted } = req.body;
  try {
    const todo = await prisma.todos.create({
      data: {
        text,
        iscompleted,
      },
    });
    res.status(201).json(todo);
  } catch (err) {
    next(err);
  }
}

// 4. Update an available id todo.
async function updateTodo(req, res, next) {
  const id = parseInt(req.params.id);
  const { text, iscompleted } = req.body;
  try {
    await validateID.validate({ inputID: id });
    await dataSchema.validate(req.body);
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const Todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });
    if (!Todo) {
      return res.send(404);
    }
    const todo = await prisma.todos.update({
      where: { id: id },
      data: { text: text, iscompleted: iscompleted },
    });
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

//5. Remove a todo from todos.
async function removeTodo(req, res, next) {
  const id = parseInt(req.params.id);

  try {
    await validateID.validate({ inputID: id });
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const Todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });

    if (!Todo) {
      return res.send(404);
    }

    const todo = await prisma.todos.delete({
      where: {
        id: id,
      },
    });
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

module.exports = { getTodos, getTodoByID, addTodo, removeTodo, updateTodo };
